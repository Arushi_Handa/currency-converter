import java.util.Scanner;

public class MainClass {
static float e;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String destCurrency;
		System.out.println("Enter the destination currency: ");
		destCurrency=sc.nextLine();
		float usdQty;
		System.out.println("Enter the quantity to be converted: ");
		usdQty= sc.nextFloat();
		boolean d = validate(usdQty,destCurrency);
		if (d == false) {
			ConverterClass convertCurrency = new ConverterClass(10,"CAD");
			convertCurrency.display();
		} else {
			System.out.println("Unable to convert the given input.");
		}
	}
static boolean validate(float usdQty, String destCurrency) {
		if ((usdQty > 0)||((destCurrency == "EUR") || (destCurrency == "INR") || (destCurrency == "MYR")|| (destCurrency == "SGD") || (destCurrency == "GBP") || (destCurrency == "CAD")))
			return false;
		else
			return true;
}
}
class USDClass {
	float usdQty;
	USDClass(float usdQty) {
		this.usdQty = usdQty;
	}
}

class ConverterClass extends USDClass {
	float f;
	String destCurrency;
	ConverterClass(float usdQty, String destCurrency) {
		super(usdQty);
		this.destCurrency = destCurrency;
}

	float convertCurrency() {
		if (destCurrency == "EUR") {
			return usdQty * 0.81f;
		} else if (destCurrency == "INR") {
			return usdQty * 64.31f;
		} else if (destCurrency == "MYR") {
			return usdQty * 3.95f;
		} else if (destCurrency == "SGD") {
			return usdQty * 1.32f;
		} else if (destCurrency == "GBP") {
			return usdQty * 0.72f;
		} else if (destCurrency == "CAD") {
			return usdQty * 1.26f;
		} else
			return usdQty;
	}

	void display() {
		System.out.println("The " + destCurrency + " amount equivalent to " + usdQty + " is: " + convertCurrency());
	}
}
